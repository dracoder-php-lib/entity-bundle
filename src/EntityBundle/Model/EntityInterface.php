<?php


namespace Dracoder\EntityBundle\Model;


interface EntityInterface
{
    public function getId();
}