<?php


namespace Dracoder\EntityBundle\Model;


use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Dracoder\Extensions\DateTimeExtension;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\MappedSuperclass()
 */
#[ORM\HasLifecycleCallbacks]
#[ORM\MappedSuperclass]
abstract class AbstractTimetrackeableEntity extends AbstractEntity
{
    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")
     */
    #[ORM\Column(type: "datetime")]
    protected DateTime $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")
     */
    #[ORM\Column(type: "datetime")]
    protected DateTime $updatedAt;

    /**
     * AbstractTimetrackeableEntity constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $now = new DateTimeExtension();
        $this->createdAt = $now;
        $this->updatedAt = $now;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(Datetime $updatedAt): AbstractTimetrackeableEntity
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new DateTime('now'));
        }
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt): AbstractTimetrackeableEntity
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
