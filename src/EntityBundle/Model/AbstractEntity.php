<?php


namespace Dracoder\EntityBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Dracoder\Uuid;

abstract class AbstractEntity implements EntityInterface
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @ORM\Id
     */
    #[ORM\Column(type: "string", length: 255)]
    #[ORM\Id]
    protected string $id;

    /**
     * AbstractEntity constructor.
     */
    public function __construct()
    {
        $id = Uuid::uuid();
        $this->id = ($id ?: '');

    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }
}
