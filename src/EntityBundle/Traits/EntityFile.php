<?php

namespace Dracoder\EntityBundle\Traits;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;

trait EntityFile
{
    use FileSystem;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[ORM\Column(type: "string", length: 255, nullable: true)]
    protected ?string $file = null;

    /** @var string|null $temp */
    private ?string $temp = '';

    /** @var bool $triggerUpdate */
    private bool $triggerUpdate = false;

    /**
     * Get file
     *
     * @return string|null
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * Set file
     *
     * @param string|null $file
     *
     * @return $this
     */
    public function setFile(?string $file): self
    {
        $this->triggerUpdate = true;
        $this->file = $file;

        return $this;
    }

    /**
     * @ORM\PreUpdate
     *
     * @param PreUpdateEventArgs $event
     */
    #[ORM\PreUpdate]
    public function beforeItChange(PreUpdateEventArgs $event): void
    {
        if ($event->hasChangedField('file')) {
            $lastValue = $event->getOldValue('file');
            if ($lastValue) {
                $this->temp = $lastValue;
            }
        }
    }

    /**
     * @ORM\PreRemove
     */
    #[ORM\PreRemove]
    public function beforeItDelete(): void
    {
        if ($this->file) {
            $this->temp = $this->file;
        }
    }

    /**
     * @ORM\PostUpdate()
     */
    #[ORM\PostUpdate]
    public function postUpdate(): void
    {
        if ($this->triggerUpdate && $this->temp && $this->temp !== $this->file) {
            $this->deleteFile();
        }
    }

    /**
     * Elimina el archivo que se encuentra en la variable temporal
     */
    private function deleteFile(): void
    {
        if ($this->temp) {
            $this->temp = $this->getProjectDir() . $this->temp;
            if (file_exists($this->temp) && is_writable($this->temp) && !is_dir($this->temp)) {
                @unlink($this->temp);
            }

            $this->temp = null;
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function postRemove(): void
    {
        $this->deleteFile();
    }
}
