<?php


namespace Dracoder\EntityBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use SplFileInfo;

trait EntityImage
{
    use EntityFile;

    private array $imageFormat = ['jpeg', 'png', 'jpg', 'gif'];

    /**
     * @ORM\PostUpdate()
     */
    #[ORM\PostUpdate]
    public function postUpdate(): void
    {
        if ($this->triggerUpdate && $this->temp && $this->temp !== $this->file) {
            $this->deleteFile();
        }
    }

    /**
     * Elimina el archivo que se encuentra en la variable temporal
     */
    private function deleteFile(): void
    {
        if ($this->temp) {

            $this->temp = $this->getProjectDir() . $this->temp;
            if (file_exists($this->temp) && is_writable($this->temp) && !is_dir($this->temp)) {
                // delete the old image
                @unlink($this->temp);
            }

            //Borramos la carpeta de thumbnails
            if ($this->isImage()) {
                $pathInfo = pathinfo($this->temp);
                $fileName = $pathInfo['basename'];
                $thumbnailsFolder = $pathInfo['filename'];
                $thumbnailsFullFolderPath = sprintf('%s%s', str_replace($fileName, '', $this->temp), $thumbnailsFolder);
                $thumbnailsFullFolderPath = str_replace("\\", "/", $thumbnailsFullFolderPath);

                self::rmDir($thumbnailsFullFolderPath);
            }

            // clear the temp image path
            $this->temp = null;
        }
    }

    /**
     * Is imagen
     *
     * @return bool
     */
    public function isImage(): bool
    {
        $extension = $this->getExtension();
        if (in_array($extension, $this->imageFormat, true)) {
            return true;
        }

        return false;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension(): string
    {
        return (new SplFileInfo($this->getFile()))->getExtension();
    }

    /**
     * Método para eliminar la carpeta especificada y su contenido
     *
     * @param string $dir
     */
    private static function rmDir(string $dir): void
    {
        if (is_dir($dir) && is_writable($dir)) {
            $selectedDir = opendir($dir);
            // Reading all subdirectory files
            while ($file = readdir($selectedDir))
            {
                if (!is_dir($dir."/".$file)) {
                    if ($dir === "./") {
                        $url = $dir.$file;
                    } else {
                        $url = $dir."/".$file;
                    }
                    if (file_exists($url) && is_writable($url) && !is_dir($url)) {
                        // delete the old image
                        @unlink($url);
                    }
                }
            }
            closedir($selectedDir);
            rmdir($dir);
        }
    }

    /**
     * @ORM\PostRemove()
     */
    #[ORM\PostRemove]
    public function postRemove(): void
    {
        $this->deleteFile();
    }
}
