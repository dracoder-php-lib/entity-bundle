<?php

namespace Dracoder\EntityBundle\Traits;

use LogicException;
use ReflectionObject;

use function dirname;

trait FileSystem
{
    /**
     * @var string|null $projectDir
     */
    private ?string $projectDir = null;

    protected function getProjectDir(): ?string
    {
        if (null === $this->projectDir) {
            $r = new ReflectionObject($this);

            if (!file_exists($dir = $r->getFileName())) {
                throw new LogicException(sprintf('Cannot auto-detect project dir for kernel of class "%s".', $r->name));
            }

            $dir = dirname($dir);
            while (true) {
                if (file_exists($dir.'/composer.json') && !str_contains($dir, 'vendor')) {
                    break;
                }
                $dir = dirname($dir);
            }
            $this->projectDir = $dir;
        }

        return $this->projectDir;
    }
}
